#include "defs.h"
#include "Game.h"
#include "Interface.h"
#undef max

int main() {

    srand(time(NULL));
    bool exit_flag = false;
    int choice, game_size, row_size;

    while (!exit_flag) {

        /* Wyświetlenie menu gry */
        gameInterface::displayMenu();

        /* Użytkownik wybiera opcje */
        choice = gameInterface::readChoice();

        /* Wykonanie opcji zależnie od wyboru użytkownika */
        /* Gra z komputerem */
        if (choice == 1) {

            game_size = gameInterface::chooseGameSize(); // Wybór rozmiaru gry np 3 -> plansza o wymiarach 3x3.
            row_size = gameInterface::chooseRowSize(game_size); // Wybór liczby elementów w rzędzie wygrywającym.
            Game game(true, game_size, row_size); // Utworzenie gry z podanymi parametrami.
            int winner = game.play();             // Rozpoczęcie gry.
            gameInterface::displayWinner(winner); // wyswietl informacje o zwyciezcy

        }
        /* Dwóch graczy. */
        else if (choice == 2) {

            game_size = gameInterface::chooseGameSize(); // wybierz rozmiar gry
            row_size = gameInterface::chooseRowSize(game_size);
            Game game(false, game_size, row_size); // utworz gre z podanymi parametrami
            int winner = game.play();              // rozpocznij gre
            gameInterface::displayWinner(winner);  // wyswietl informacje o zwyciezcy

        }
        else if (choice == 3) {
            exit_flag = true;
            gameInterface::clearScreen();
        }
    }

    return 0;
}
