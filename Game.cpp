#include "Game.h"

/* zwraca przeciwny numer gracza, 1 dla 2, 2 dla 1 */
int Game::OP(int turn) { return turn % 2 + 1; }

/* Sprawdza czy podany punkt należy do planszy. Jeśli tak to zwraca 1. */
bool Game::inRange(const int x, const int y) {
    return x >= 1 && y >= 1 && x <= game_size && y <= game_size;
}

/* Funkcja sprawdza czy podany gracz wygrał
     jako parametr podany jest gracz i jego ostatni ruch. */
bool Game::check(const int player, const int px, const int py) {
    int repeats, x, y;

    /* Sprawdzenie kolumn
   W danej kolumnie sprawdzamy kolejne pola, gdy row_size znaków się powtórzyło z rzędu to zwycięstwo. 
   Gdy natrafimy na jakiś inny znak to zerujemy zmienna repeats.                           */
    repeats = 0;
    const int shift = row_size - 1;
    for (y = py + shift, x = px; y >= py - shift; y--)
        if (inRange(x, y) && board[x][y] == player) {
            if (++repeats == row_size)
                return true;
        }
        else
            repeats = 0;


    /* Sprawdzenie wierszy
   W danym wierszy sprawdzamy kolejne pola, gdy row_size znaków się powtórzyło z rzędu to zwycięstwo.
   Gdy natrafimy na jakiś inny znak to zerujemy zmienna repeats.                           */
    repeats = 0;
    for (x = px - shift, y = py; x <= px + shift; x++)
        if (inRange(x, y) && board[x][y] == player) {
            if (++repeats == row_size)
                return true;
        }
        else
            repeats = 0;

    /* Sprawdzenie przekątnych ,,/'' */
    repeats = 0;
    for (y = py + shift, x = px - shift; y >= py - shift; y--, x++)
        if (inRange(x, y) && board[x][y] == player) {
            if (++repeats == row_size)
                return true;
        }
        else
            repeats = 0;

    /* Sprawdzenie przekątnych ,,\'' */
    repeats = 0;
    for (y = py + shift, x = px + shift; y >= py - shift; y--, x--)
        if (inRange(x, y) && board[x][y] == player) {
            if (++repeats == row_size)
                return true;
        }
        else
            repeats = 0;

    return false;
}

/* Funkcja oblicza najkorzystniejszy ruch (do podanej głębokości) za pomocą algorytmu minimax.
    Rysuje drzewo o ''MAX_MINIMAX_DEPTH[game_size]'' poziomach. symuluje każdą możliwą opcje w tych poziomach.
    W naszym programie komputer jest zawsze Krzyżykiem, dlatego też drzewo gry będzie rysowane
    z punktu widzenia Krzyżyka, a więc:
    Gdy ruch należy do Kółka, funkcja będzie szukała ruchu o jak najmniejszej wartości. (Minimizer)
    Gdy ruch należy do Krzyżyka, funkcja będzie szukała ruchu o jak największej wartości. (Maximizer) */
int Game::minimax(const int depth, const int player) {

    /* Gdy ruch dla Kółka przypisz 20, gdy dla Krzyżyka to -10. */
    int best_value = (player == 1 ? 20 : -10), value;
    /* Do struktury local_best_move będziemy przypisawać najkorzystniejszy ruch dla gracza, którego jest obecnie kolej. */
    std::pair<int, int> local_best_move = { 0, 0 };

    /* Przeglądanie wszystkich możliwych ruchów. */
    for (int y = 1; y <= game_size; y++)
        for (int x = 1; x <= game_size; x++) {
            /* Jeśli dane pole jest już zajęte, pomiń je. */
            if (board[x][y])
                continue;
            /* Symulacja wykonania ruchu. */
            board[x][y] = player;

            /* Sprawdzanie skutków ruchu */

            /* Minimizer */
            /* Gdy kolej Kółka to znajdź najlepszy dla niego ruch, jednocześnie najgorszy dla Krzyżyka. */
            if (player == 1) { 
                /* Jeśli Kółko wygrało to do wartości value przypisz depth, ponieważ depth
                będzie napewno mniejsze od 20(tyle wynosi aktualnie zmienna best_value), a w rezultacie
                funkcja minimax() zwróci ten ruch jak najbardziej korzysty dla Kółka.        */
                if (check(1, x, y))
                    value = depth;
                /* Jeśli wykonanie danego ruchu nie przyniosło zwycięstwa dla Kółka, to wywyołaj
                funkcję minmax() dla większej głębokości, jeśli to możliwe. Innymi słowy zasymuluj kolejny ruch.
                 W przypadku gdy nie możemy już wykonać kolejnej symulacji, to w pętli for będziemy sprawdzać, czy któreś pole zapewni
               zwycięstwo Kółku, jeśli żadne pole nie da zwycięstwa, to funkcja zwróci ostatnie pole zapisane w strukturze local_best_move.*/
                else if (depth < MAX_MINIMAX_DEPTH[game_size - 1])
                    value = minimax(depth + 1, 2);
                /* Jeśli Kółko nie wygrało w żadnej z zasymulowanych rund to value=5. Jest to wartość mniejsza od 20,
                co spowoduje zwrócenie obecnego ruchu jak najkorzystniejszy dla kółka, mimo że nie zapewni on wygranej. Poniższy else
                wykona się dopiero po MAX_MINIMAX_DEPTH[game_size - 1] rundach. */
                else
                    value = 5;
                /* Jeśli znaleziono lepszy ruch, przypisz go do local_best_move */
                if (value < best_value) {
                    best_value = value;
                    local_best_move = { x, y };
                }

            }
            /* Maximizer */
           /* Gdy kolej Krzyżyka to znajdź najlepszy dla niego ruch, jednocześnie najgorszy dla Kółka. */
            else { 

                /* Jeśli Krzyżyk wygrał to do wartości value przypisz 10-depth, ponieważ 10-depth
                będzie napewno większe od -10(tyle wynosi aktualnie zmienna best_value), a w rezultacie
                funkcja minimax() zwróci ten ruch jak najbardziej korzysty dla Krzyżyka.        */
                if (check(2, x, y))
                    value = 10 - depth;
                /* Jeśli wykonanie danego ruchu nie przyniosło zwycięstwa dla Krzyżyka, to wywyołaj
               funkcję minmax() dla większej głębokości, jeśli to możliwe. Innymi słowy zasymuluj kolejny ruch.
               W przypadku gdy nie możemy już wykonaj kolejnej symulacji, to w pętli for będziemy sprawdzać, czy któreś pole zapewni
               zwycięstwo Krzyżykowi, jeśli żadne pole nie da zwycięstwa, to funkcja zwróci ostatnie pole zapisane w strukturze local_best_move.*/
                else if (depth < MAX_MINIMAX_DEPTH[game_size - 1])
                    value = minimax(depth + 1, 1);
                /* Jeśli Krzyżyk nie wygrał w żadnej z zasymulowanych rund to value=5. Jest to wartość większa od -10,
                co spowoduje zwrócenie obecnego ruchu jak najkorzystniejszy dla Krzyżyka, mimo że nie zapewni on wygranej. Poniższy else
                wykona się dopiero po MAX_MINIMAX_DEPTH[game_size - 1] rundach. */
                else
                    value = 5;
                /* Jeśli znaleziono lepszy ruch, przypisz go do local_best_move */
                if (value > best_value) {
                    best_value = value;
                    local_best_move = { x, y };
                }
            }
            /* Cofnij symulowany ruch. */
            board[x][y] = 0; 
        }

    /* Jeśli brak możliwości ruchu to remis. */
    if (best_move.first == 0)
        best_value = 5;
    best_move = local_best_move;

    return best_value;
}

/* konstruktor, do którego należy podać: typ gry, rozmiar planszy, rozmiar rzędu wygrywającego.
Argumenty podane w konstruktorze są przypisywane od razu do odpowiednich atrybutów klasy Game. */
Game::Game(const bool in_singleplayer, const int in_game_size, const int in_row_size)
    : singleplayer(in_singleplayer), game_size(in_game_size), row_size(in_row_size) {

    /* Alokacja pamięci dla planszy o podanym wymiarze oraz 
     wypełnienie calej tablicy zerami, bo na początku wszystkie pola są puste */
    board = new int* [game_size + 1];
    for (int i = 0; i <= game_size; i++) {
        board[i] = new int[game_size + 1];
        for (int j = 0; j <= game_size; j++)
            board[i][j] = 0;
    }
}

/* destruktor - odpowada za zwolnienie pamieci. */
Game::~Game() {
    for (int i = 0; i < game_size; i++)
        delete[] board[i];
    delete[] board;
}

/* Główna funkcja gry, zwraca informację o zwycięzcy: 0-remis, 1-kółko, 2-krzyżyk */
int Game::play() {
    /* Losowanie gracza rozpoczynającego (1 lub 2) */
    turn = rand() % 2 + 1; 
    /* Póki nie ma zwycięzcy lub remisu */
    while (!winner) {
        /* Wyświetlanie planszy z aktualnym stanem gry */
        gameInterface::printBoard(board, game_size); 
        /* Jeśli jest ruch dla kółka (bo komputer jest zawsze krzyżykiem) lub jeśli jest dwóch graczy. */
        if (turn == 1 || !singleplayer) {
            /* Wczytanie ruchu od użytkownika. */
            move = gameInterface::readMove(game_size,turn); 
        /* Ruch niepoprawny jeśli pole jest juz zajęte. */
            if (move.first && board[move.first][move.second])
                move = { 0, 0 };
        }
        else {
            /* Jeśli komputer ma wykonać ruch, uruchommy algorytm minmax. */
            minimax(0, turn);
            /* Najlepszy ruch znaleziony przez algorytm minmax zostaje przypisany do zmiennej move. */
            move = best_move;
        }

        /* Jeśli ruch jest niepoprawny, to wyświetl błąd i wczytaj ruch ponownie */
        if (move.first == 0) {
            gameInterface::printError();
            gameInterface::wait(1);
            /* Polecenie continue spowoduje przejście do kolejnego obiegu pętli bez wykonywania pozostałych linii kodu. */
            continue;
        }
        /* Przypisz do wybranego pola symbol gracza wykonującego obecnie ruch */
        board[move.first][move.second] = turn;  
        /* Sprawdzenie czy gra została zakończona. */
        if (check(turn, move.first, move.second)) 
            winner = turn;
        /* Kolej na ruch drugiego gracza */
        turn = OP(turn);
        /* Jeśli wykonano ruch, do zmiennej used dodajemy 1. Gdy zmienna ta przyjmie wartość równą ilością pół w grze to przerywamy pętle while.
        */
        if (++used == game_size * game_size)
            break;
    }
    /* wyswietla plansze z aktualnym stanem gry. */
    gameInterface::printBoard(board, game_size);
    return winner;
}
