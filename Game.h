#pragma once
#ifndef GAME_H
#define GAME_H

#include "defs.h"
#include "Interface.h"

/* klasa Game odpowiada za rozgrywkę. */
class Game {
private:
    /* Dwuwymiarowa tablica dynamiczna, ktora zapisuje obecny stan gry
    0 - puste pole, 1 - kółko, 2 - krzyżyk */
    int** board;
    const bool singleplayer;  // 0 (false) jeśli jest dwóch graczy, 1 (true) jeśli gra z komputerem.
    const int game_size;      // Rozmiar planszy.
    const int row_size;       // Rozmiar wygrywającego rzędu.
    /* Czym jest pair? 
    Struktura, która umożliwia traktowanie dwóch obiektów jako jednego obiektu.
    Do tak utworzonego obiektu odwołujemy sie w następujący sposób:
    move.first=2;
    move.second=5;   */
    std::pair<int, int> move; // Informacja o wykonywanym ruchu.
    int turn;                 // Czyj jest obecnie ruch: 1-kółko, 2-krzyżyk.
    int winner = 0;           // Kto wygrał grę: 1-kółko, 2-krzyżyk, 0-remis
    int used = 0;             // Ile pól jest zajetych.
    std::pair<int, int> best_move; // Przechowuje informację o najlepszym znalezionym ruchu przez algorthm minimax.

    /* zwraca przeciwny numer gracza, 1 dla 2, 2 dla 1 */
    int OP(int turn);

    /* sprawdza czy podany punkt nalezy do planszy. */
    bool inRange(const int x, const int y);

    /* Funkcja sprawdza czy podany gracz wygrał
     jako parametr podany jest gracz i jego ostatni ruch. */
    bool check(const int player, const int px, const int py);

   /* Funkcja oblicza najkorzystniejszy ruch (do podanej głębokości) za pomocą algorytmu minimax. 
      Rysuje drzewo o ''MAX_MINIMAX_DEPTH[game_size]'' poziomach. symuluje każdą możliwą opcje w tych poziomach.
      W naszym programie komputer jest zawsze Krzyżykiem, dlatego też drzewo gry będzie rysowane
      z punktu widzenia Krzyżyka, a więc:
      Gdy ruch należy do Kółka, funkcja będzie szukała ruchu o jak najmniejszej wartości. (Minimizer)
      Gdy ruch należy do Krzyżyka, funkcja będzie szukała ruchu o jak największej wartości. (Maximizer) */
    int minimax(const int depth, const int player); 

public:
    /* konstruktor, do którego należy podać: typ gry, rozmiar planszy, rozmiar rzędu wygrywającego. */
    Game(const bool in_singleplayer, const int in_game_size, const int in_row_size);

    /* Destruktor - odpowada za zwolnienie pamięci */
    ~Game();

    /* Główna funkcja gry, zwraca informację o zwycięzcy: 0-remis, 1-kółko, 2-krzyżyk */
    int play();
};


#endif
