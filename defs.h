// w tym pliku naglowkowym znajduja sie elementy globalne, wspolne dla wszystkich
// innych plikow, czyli m. in. biblioteka standardowa
// oraz stale constepxr, ktore pozwalaja na latwa zmiane parametrow programu

#ifndef DEFS_H
#define DEFS_H

// elementy bibilioteki standardowej wykorzystane w programie

#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iostream>
#include <limits>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <utility>


#include <conio.h>
#include <windows.h>


/* Maksymalna głębokość przeszukiwania algorytmu minimax dla poszczegolnych rozmiarow planszy */
/* constexpr wskazuje, że wartość lub wartość zwracana jest const Ant i, jeśli jest to możliwe, jest obliczana w czasie kompilacji. */
constexpr int MAX_MINIMAX_DEPTH[10] = { 3, 3, 3, 2, 2, 2, 2, 2, 1, 1 };

/* Co oznacza głębokość?
Jest to ilość ruchów do przodu symulowanych przez algorytm minmax*/

#endif
#pragma once

