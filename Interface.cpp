
#include "Interface.h"
#undef max

/*Funkcja czyści ekran konsoli i przestawia kursor do punktu (0,0) */
void gameInterface::clearScreen() {

    system("cls");

}

/* Funkcja zatrzymuje program na określony czas podany w sekundach. */
void gameInterface::wait(int wait_time) {

    Sleep(wait_time * 1000);

}

/* Funkcja wyświetla menu gry. Gracz może wybrać opcję odnośnie najbliższej rozgrywki. */
void gameInterface::displayMenu() {
    clearScreen();
    std::cout << std::endl << std::endl << "\tKOLKO I KRZYZYK" << std::endl << std::endl;
    std::cout << "\t[1] - Jeden gracz" << std::endl;
    std::cout << "\t[2] - Dwoch graczy" << std::endl;
    std::cout << "\t[3] - Wyjdz z gry" << std::endl;
}

/* pobiera od uzytkownika informacje o wyborze w menu gry
 zwraca 1, 2, 3 - zaleznie od wyboru lub 0 jesli byl on niepoprawny i konieczne
 jest powtorzenie */
int gameInterface::readChoice() {
    int input;
    /* Wczytanie opcji od użytkownika */
    std::cout << "\n\t";
    /* Gdy użytkownik nieprawidłowo wprowadzi opcję, zwróć 0. */
    if (!(std::cin >> input))
        input = 0;

    /* Zabezpieczenie na wypadek gdyby uzytkownik wpisal np. litery
     bez tego program moglby sie zawiesic
     wszystko co nie jest poprawnym wyborem jest ignorowane. */

    std::cin.clear(); // Czyszczenie flagi błędu  
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // Ignorowanie wszystkich znaków, chyba że szybciej skończy się wiersz.  

    return input;
}

/* Funkcja pozwala użytkownikowi wybrać rozmiar gry w zakresie <3; 10>, zwraca wybraną wartość. */
int gameInterface::chooseGameSize() {

    int game_size;
    bool correct_input = false;
    while (!correct_input) {
        clearScreen();
        std::cout << "\n\n\tWybierz rozmiar gry 3 - 10" << std::endl;
        game_size = readChoice();
        /* Jeśli użytkownik wybrał poprawnie rozmiar planszy z przedziału <3;10>, wyjdź z pętli while. */
        if (game_size >= 3 && game_size <= 10)
            correct_input = true;
    }
    return game_size;
}

/* Funkcja pozwala użytkownikowi wybrać liczbę elementów w wygrywającym rzędzie, zwraca wybraną wartość */
int gameInterface::chooseRowSize(int game_size) {

    int row_size;
    bool correct_input = false;
    while (!correct_input) {
        clearScreen();
        std::cout << "\n\n\tWybierz rozmiar rzedu wygrywajacego 3 - ";
        std::cout << game_size << std::endl;
        row_size = readChoice();
        if (row_size >= 3 && row_size <= game_size)
            correct_input = true;
    }
    return row_size;
}

/* Funkcja wyświetla planszę z aktualnym stanem gry. */
void gameInterface::printBoard(int** board, int game_size) {
    clearScreen();

   /* Wyświetlnie górnego rzędu liter */
    std::cout << "\n\n        ";
    for (int x = 0; x < game_size; x++)
        std::cout << static_cast<char>('A' + x) << " ";
    std::cout << std::endl;

    /* Wyświetlenie planszy wierszami */
    for (int y = game_size; y; y--) {

        /* Wyświetlenie numeru wiersza, wyjątek dla numeru 10 */
        if (y < 10)
            std::cout << "      " << y;
        else
            std::cout << "     10";

        /* Wyświetlenie wiersza (dane z tablicy board) */
        for (int x = 1; x <= game_size; x++) {
            std::cout << " ";

            /* Gdy pole jest puste */
            if (!board[x][y]) 
                std::cout << " ";
            /* Gdy kółko */
            else if (board[x][y] == 1) { 
                std::cout << "o";
            }
            /* Gdy krzyżyk */
            else if (board[x][y] == 2) { 
                std::cout << "x";
            }
        }
        std::cout << std::endl;
    }
}


/* Funkcja sprawdza czy współrzędna należy do planszy o podanym rozmiarze. */
bool gameInterface::checkRange(int x, int game_size) {
    return x >= 1 && x <= game_size;
}

/* Funkcja przekształca literę na odpowiedni numer kolumny */
int gameInterface::CTI(char x) {
    if (x >= 'a' && x <= 'z')
        /* A->1, B->2 .. */
        return x - 'a' + 1;
    else if (x >= '0' && x <= '9')
        /* 0 ->0 1->1 .. */
        return x - '1' + 1;
    else
        return 0;
}

/* Wczytanie ruchu podanego przez użytkownika. */
std::pair<int, int> gameInterface::readMove(int game_size, int turn) {

    std::cout << "\n\n\tRuch gracza " << (turn == 1 ? 'o' : 'x') << ":  ";
    std::pair<int, int> result;
    std::string input;
    /* Wczytanie wejścia od użytkownika */
    std::cin >> input; 
    /* Zabezpieczenie, gdyby użytkownik wpisał niepoprawny ciąg znaków. */
    std::cin.clear(); // Czyszczenie flagi błędu.
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // Ignorowanie podanej liczby znaków, chyba że wcześniej będzie koniec wiersza.

    /* Przetwarzanie wejścia użytkownika.
     Sprawdzanie poprawności i zamiana na współrzędne pola na planszy. */

     /* Ominięcie początkowych białych znaków */
    int r = 0;
    while (input[r] == '\t' || input[r] == ' ')
        ++r;

    bool correct_format = true;

    /* Wczytanie litery jako współrzędnej x */
    /* Funkcja isalpha() - sprawdza czy znak przekazany jako argument jest literą alfabetu. Zwraca 0 jeśli nie jest, a jeśli jest to wartość różną od zera. */
    if (isalpha(input[r]) && checkRange(CTI(input[r]), game_size))
        result.first = CTI(input[r++]);
    else
        correct_format = false;

    /* Wczytanie liczby jako współrzędnej y, wyjątek dla dwucyfrowej liczby 10 */
    /* Funkcja isdigit() - sprawdza czy znak przekazany jako argument jest cyfrą. Zwraca 0 jeśli nie jest, a jeśli jest to wartość różną od zera. */
    if (isdigit(input[r]) && checkRange(CTI(input[r]), game_size)) {
        if (game_size == 10 && input[r] == '1' && input[r + 1] == '0') {
            result.second = 10;
            r += 2;
        }
        else
            result.second = CTI(input[r++]);
    }
    /* Jeśli nie wprowadzono litery, następnie liczby to błędny format. */
    else
        correct_format = false;

    /* Jeśli wprowadzono zły format. Przypisz zera. */
    if (!correct_format)
        result = { 0, 0 };

    return result;
}

/* Funkcja wyświetla informację o błędnie wpisanym ruchu. */
void gameInterface::printError() { std::cout << "\n\truch niepoprawny\n"; }

/* wyswietla informacje o zwyciezcy gry */
void gameInterface::displayWinner(int winner) {

    wait(1);
    clearScreen();
    std::cout << "\n\n\t";
    if (winner == 1) {
        std::cout << "Wygral O!\n";
    }
    else if (winner == 2) {
        std::cout << "Wygral X!\n";
    }
    else if (winner == 0)
        std::cout << "Remis!\n";

    wait(2);
}
