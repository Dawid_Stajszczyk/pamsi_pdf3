
#pragma once
#ifndef INTERFACE_H
#define INTERFACE_H

#include "defs.h"

/* W tej przestrzeni nazw znajduja sie funkcje zwiazane z interfejsem gry,
   czyli wyswietlajace gre oraz pobierajace wejscie od uzytkownika */

namespace gameInterface {
	

/*Funkcja czyści ekran konsoli i przestawia kursor do punktu (0,0) */
	void clearScreen();

	/* Funkcja zatrzymuje program na określony czas podany w sekundach. */
	void wait(int time);

	/* Funkcja wyświetla menu gry. Gracz może wybrać opcję odnośnie najbliższej rozgrywki. */
	void displayMenu();

	/* pobiera od uzytkownika informacje o wyborze w menu gry
	 zwraca 1, 2, 3 - zaleznie od wyboru lub 0 jesli byl on niepoprawny i konieczne
	 jest powtorzenie */
	int readChoice();

	/* Funkcja pozwala użytkownikowi wybrać rozmiar gry w zakresie <3; 10>, zwraca wybraną wartość. */
	int chooseGameSize();

	/* Funkcja pozwala użytkownikowi wybrać liczbę elementów w wygrywającym rzędzie, zwraca wybraną wartość */
	int chooseRowSize(int game_size);

	/* Wyświetla planszę z aktualnym stanem gry. */
	void printBoard(int** board, int game_size);

	/* Funkcja sprawdza czy współrzędna należy do planszy o podanym rozmiarze. */
	bool checkRange(int x, int game_size);

	/* Funkcja przekształca literę na odpowiedni numer kolumny */
	int CTI(char x);

	/* Wczytanie ruchu podanego przez użytkownika. */
	std::pair<int, int> readMove(int game_size, int turn);

	/* Funkcja wyświetla informację o błędnie wpisanym ruchu. */
	void printError();

	/* Wywietla informację o zwycięzcy gry. */
	void displayWinner(int winner);

} // namespace gameInterface

#endif
